class CreateLectures < ActiveRecord::Migration
  def change
    create_table :lectures do |t|
      t.text :lectname
      t.text :lecttext
      t.integer :lecture_id
      t.references :user, index: true

      t.timestamps
    end
  end
end
