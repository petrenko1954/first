# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
FirstApp::Application.config.secret_key_base = 'bef3da2cce80ec49f38e02235d9564e98fde5ae577abf7588dfd0384e1fd229adb5b7bb8cf4ff0716fa9e2112bff70dffc6e93a16a29742e350847e236c8422c'
